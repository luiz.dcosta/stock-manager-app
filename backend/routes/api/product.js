const express = require('express')
const router = express.Router();
const { check, validationResult } = require('express-validator')
const Produto = require('../../models/product')
const User = require('../../models/user');
const auth = require('../../middleaware/auth')

router.get('/', auth, async(req, res, next) =>{
    try{
      const produto = await Produto.find({})
      
      if(produto){
        res.json(produto)
      }else{
        res.status(404).send({"error":"Product not found"})
      }

    }catch(err){
      console.error(err.message)
      res.status(500).send({"error":"Server error"})
    }
})

router.post('/', [
  check('serial').not().isEmpty(),
  check('modelo').not().isEmpty(),
  check('marca').not().isEmpty(),
  check('valor').not().isEmpty(),
  check('quantidade').not().isEmpty(),
  check('descricao').not().isEmpty()
], auth, async(req, res, next) =>{
    try{
      let {serial, modelo, marca, valor, quantidade, descricao} = req.body

      const errors = validationResult(req)
      
      if(!errors.isEmpty()){
        return res.status(400).json({ errors: errors.array()})
      }else{
        let produto = new Produto({serial, modelo, marca, valor, quantidade, descricao})

        await produto.save()

        if(produto.id){
          res.json(produto)
        }
      }
    }catch(err){
      console.log(err.message)
      res.status(500).send({"error": "Server Error"})
    }
  })

router.patch('/:serial', [], auth, async (req, res, next) => {
  try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      res.status(400).send({ errors: errors.array() })
      return
    }
    const serial = req.params.serial
    
    let bodyRequest = req.body

    console.log(bodyRequest)
    const update = { $set: bodyRequest }
    const produto = await Produto.findOneAndUpdate({serial:serial}, update, { new: true })
    if (produto) {
      res.send(produto)
    } else {
      res.status(404).send({ error: "Product doesn't exist" })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": "Server Error" })
  }
})

router.delete('/:serial', [], auth, async (req, res, next) => {
  try {
    let serial = serial.params["serial"]
    const product = await Produto.findOneAndDelete({serial:serial})
    if (product) {
      res.send(product)
    } else {
      res.status(404).send({ "error": "Serial not found" })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": "Server Error" })
  }    
})

module.exports = router;