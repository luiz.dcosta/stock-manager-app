const bcrypt = require('bcryptjs');
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    serial:{
        type:String,
        required:true,
        unique:true
    },
    modelo:{
        type:String,
        required:true
    },
    marca:{
        type:String,
        required:true
    },
    valor:{
        type:String,
        required:true,
    },
    quantidade:{
        type:String,
        required:true
    },
    descricao:{
        type:String,
        required:true
    },
    date:{
        type:Date,
        default: Date.now
    }
})

module.exports = mongoose.model('produto', UserSchema)