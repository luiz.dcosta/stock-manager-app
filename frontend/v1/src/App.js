import React, {useState} from 'react';
import './App.css';

import Routes from './routes'

function App() {
  const [cadastro, setCadastro] = useState("Stock Manager")
   
  const handleCadastro = () => {
    setCadastro("Cadastrado")
  }
  
  const handleVoltar = () => {
    setCadastro("Voltar")
  }  

  return (
    <div>
      <header>
            <h1>{cadastro}</h1>
      </header>
      <Routes/>
    </div>
  ) 
}

export default App;