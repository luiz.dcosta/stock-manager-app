import { clientHttp } from '../config/config.js'

/*const createUser = (data) => clientHttp.post(`/routes/api/user`, data)

const ListUser = () => clientHttp.get(`/routes/api/user`)

const DeleteUser = (email) => clientHttp.delete(`/routes/api/user/${email}`)

const createProduct = (data) => clientHttp.post('/routes/api/products', data)

const ListProduct = () => clientHttp.get(`/routes/api/products`)

const DeleteProduct = (serial) => clientHttp.delete(`/routes/api/products/${serial}`)
*/

const showUserId = (id) => clientHttp.patch(`/routes/api/user/${id}`)
const createUser = (data) => clientHttp.post(`/routes/api/user`, data)
const updateUser = (data) => clientHttp.patch(`/routes/api/user/${data._id}`, data)
const ListUser = () => clientHttp.get(`/routes/api/user`)
const DeleteUser = (id) => clientHttp.delete(`/routes/api/user/${id}`)

export {
    /*createUser,
    ListUser,
    DeleteUser,
    createProduct,
    ListProduct,
    DeleteProduct,*/
    
    createUser,
    ListUser,
    DeleteUser,
    showUserId,
    updateUser
}